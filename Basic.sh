echo $0

cat /etc/shells

echo "Hello World!"

pwd

ls

mkdir myscripts # create a new file named

ls

vi 01_basic.sh

cat 01_basic.sh

# for finding the name of the shell script
# we have shebang which is #!/bin/bash 

# rwx

# Run using 
# ./script.sh
# /path/script/sh
# bash script.sh
# CTRL+C TO CTRL+Z FOR TERMINATING AND STOPING RESPECTIVELY

bash 01_basics.sh

chmod +x 01_basic.sh

ls -ltr

./01_basic.sh

ls

pwd

ls

/home/paul/myscripts/01_basics.sh

# Using # This is comment
# Multiline comment
# <<comment
# ....
# your comment here
# ....
# comment

vi 02_comment.sh

echo "Checking Comments"

bash 02_comments.sh

cat 02_comments.sh










